//
//  ViewController.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Variables
    var game = Set()
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var deckLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var bestTimeLabel: UILabel!
    @IBOutlet weak var cardsUIView: CardsUIView!
    @IBOutlet weak var dealButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleViewRotation(orientation: UIApplication.shared.statusBarOrientation)
        startGame()
        updateDeckLabel(deck: game.deckCount)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        handleViewRotation(orientation: toInterfaceOrientation)
    }
    
    //MARK: - Actions
    @IBAction func dealMoreCardsButton() {
        guard game.addMoreCards() else { return }
        cardsUIView.addCards(numberOfCards: 3)
        for index in game.cardsOnPlay.count-3..<game.cardsOnPlay.count {
            setUpCard(cardButton: cardsUIView.cardsButtons[index], index: index)
        }
    }
    
    @IBAction func showSet() {
        game.findSet(check: false)
    }
    
    @IBAction func restartButton() {
        game = Set()
        dealButton.isEnabled = true
        dealButton.alpha = 1
        startGame()
    }
    
    @objc func touchCard(recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state else { return }
        guard let cardButton = recognizer.view as? CardButton,
            cardsUIView.cardsButtons.firstIndex(of: cardButton) != nil else { return }
        game.selectCard(index: cardsUIView.cardsButtons.firstIndex(of: cardButton)!)
    }
    
    @objc func swipeDownCards(recognizer: UISwipeGestureRecognizer) {
        dealMoreCardsButton()
    }
    
    private func addGestureRecognizer(cardButton: CardButton) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(touchCard(recognizer: )))
        cardButton.addGestureRecognizer(tap)
        addDealCardsGestureRecognizer()
    }
    
    private func addDealCardsGestureRecognizer() {
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDownCards(recognizer: )))
        swipeGestureRecognizer.direction = .down
        view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    private func startGame() {
        game.setDelegate = self
        updateDeckLabel(deck: game.deckCount)
        startDeal()
        startTimer()
    }
    
    //MARK: - Deal cards
    private func startDeal() {
        cardsUIView.removeButtons()
        cardsUIView.cardsButtons.removeAll()
        cardsUIView.addCards(numberOfCards: game.cardsOnPlay.count)
        for index in 0..<game.cardsOnPlay.count {
            setUpCard(cardButton: cardsUIView.cardsButtons[index], index: index)
        }
    }
    
    private func setUpCard(cardButton: CardButton, index: Int) {        
        cardButton.generateCardView(card: game.cardsOnPlay[index]!)
        cardButton.isHidden = false
        addGestureRecognizer(cardButton: cardButton)
    }
    
    //MARK: - Timer
    func startTimer() {
        game.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.game.timeCounter += 1
        })
    }
    
    //MARK: - Rotation controls
    private func handleViewRotation(orientation: UIInterfaceOrientation) -> Void {
        switch orientation {
        case .portrait, .portraitUpsideDown: setUpAxises(firstly: .vertical, secondary: .horizontal)
        case .landscapeLeft, .landscapeRight: setUpAxises(firstly: .horizontal, secondary: .vertical)
        default: break
        }
    }
    
    private func setUpAxises(firstly: NSLayoutConstraint.Axis, secondary: NSLayoutConstraint.Axis) {
        mainStackView.axis = firstly
        buttonsStackView.axis = secondary
    }
    
}

//MARK: - Extension. Delegation
extension ViewController: SetDelegate {
    
    func updateCard(index: Int, isSelected: Bool) {
        if isSelected {
            cardsUIView.cardsButtons[index].updateBorderColor(state: .selected)
        } else {
            cardsUIView.cardsButtons[index].updateBorderColor(state: .normal)
        }
    }
    
    func setUpNewCardsInstead(indexes: [Int]) {
        for index in indexes {
            if let card = game.cardsOnPlay[index] {
                cardsUIView.cardsButtons[index].generateCardView(card: card)
                updateCard(index: index, isSelected: card.isSelected)
                cardsUIView.cardsButtons[index].isHidden = false
            } else {
                cardsUIView.cardsButtons[index].isHidden = true
            }
        }
    }
    
    func showFindedSet(indexes: [Int]) {
        for index in indexes {
            cardsUIView.cardsButtons[index].updateBorderColor(state: .finded)
        }
    }
    
    func updateDeckLabel(deck: Int) {
        deckLabel.text = "Deck: \(deck)"
        guard deck == 0 else { return }
        dealButton.isEnabled = false
        dealButton.alpha = 0
    }
    
    func updateTimerLabel(timeCount: Int) {
        timerLabel.text = "\(timeCount.time)"
    }
    
    func updateBestTimeLabel(timeCount: Int) {
        bestTimeLabel.text = "Best: \(timeCount.time)"
    }
    
}
