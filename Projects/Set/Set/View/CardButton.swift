//
//  CardButton.swift
//  Set
//
//  Created by Marina on 4/26/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

class CardButton: UIButton {
    
    var symbol: Symbol? { didSet { setNeedsDisplay() }}
    var countOfElements = 0 { didSet { setNeedsDisplay() }}
    var color: UIColor? { didSet { setNeedsDisplay() }}
    var shading: Shading? { didSet { setNeedsDisplay() }}
    var path: UIBezierPath?
    
    private var pathRect: CGRect {
        return CGRect(x: frame.size.width * 0.1,
                      y: frame.size.height * 0.05,
                      width: frame.size.width * 0.8,
                      height: frame.size.height * 0.9)
    }
    
    private var symbolHorizontalMargin: CGFloat {
        return pathRect.width * 0.05
    }
    
    private var symbolVerticalMargin: CGFloat {
        return pathRect.height * 0.05 + pathRect.origin.y
    }
    
    private var symbolWidth: CGFloat {
        return (pathRect.width - (2 * symbolHorizontalMargin)) / 3
    }
    
    private var symbolHeight: CGFloat {
        return pathRect.size.height * 0.9
    }
    
    private var symbolsCenter: CGPoint {
        return CGPoint(x: bounds.width / 2, y: bounds.height / 2)
    }
    
    enum BorderType {
        case normal
        case selected
        case finded
    }
    
    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 0.03 * frame.size.width
        self.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.layer.cornerRadius = 10
        guard let symbol = symbol else { return }
        guard let color = color else { return }
        guard let shading = shading else { return }
        guard countOfElements <= 3 || countOfElements > 0 else { return }
        
        switch symbol {
        case .squiggle:
            drawSquiggles(count: countOfElements)
        case .diamond:
            drawDiamonds(count: countOfElements)
        case .oval:
            drawOvals(count: countOfElements)
        case .empthy:
            return
        }
        
        path!.lineCapStyle = .round
        
        switch shading {
        case .solid:
            color.setFill()
            path!.fill()
            
        case .open:
            color.setStroke()
            path!.lineWidth = 0.01 * frame.size.width
            path!.stroke()
            
        case .striped:
            path!.lineWidth = 0.01 * frame.size.width
            color.setStroke()
            path!.stroke()
            path!.addClip()
            
            var currentX: CGFloat = 0
            
            let stripedPath = UIBezierPath()
            stripedPath.lineWidth = 0.008 * frame.size.width
            
            while currentX < frame.size.width {
                stripedPath.move(to: CGPoint(x: currentX, y: 0))
                stripedPath.addLine(to: CGPoint(x: currentX, y: frame.size.height))
                currentX += 0.03 * frame.size.width
            }
            
            color.setStroke()
            stripedPath.stroke()
            
            break
        case .empthy:
            return
        }
    }
    
    // MARK: - Draw shapes
    
    // Draw squiggles
    private func drawSquiggles(count: Int) {
        let path = UIBezierPath()
        let allSquigglesWidth = CGFloat(countOfElements) * symbolWidth + CGFloat(countOfElements - 1) * symbolHorizontalMargin
        let beginX = (frame.size.width - allSquigglesWidth) / 2
        
        for i in 0..<countOfElements {
            let currentShapeX = beginX + (symbolWidth * CGFloat(i)) + (CGFloat(i) * symbolHorizontalMargin)
            let currentShapeY = symbolVerticalMargin
            let curveXOffset = symbolWidth * 0.35
            
            path.move(to: CGPoint(x: currentShapeX, y: currentShapeY))
            path.addCurve(to: CGPoint(x: currentShapeX, y: currentShapeY + symbolHeight),
                          controlPoint1: CGPoint(x: currentShapeX + curveXOffset, y: currentShapeY + symbolHeight / 3),
                          controlPoint2: CGPoint(x: currentShapeX - curveXOffset, y: currentShapeY + (symbolHeight / 3) * 2))
            path.addLine(to: CGPoint(x: currentShapeX + symbolWidth, y: currentShapeY + symbolHeight))
            path.addCurve(to: CGPoint(x: currentShapeX + symbolWidth, y: currentShapeY),
                          controlPoint1: CGPoint(x: currentShapeX + symbolWidth - curveXOffset, y: currentShapeY + (symbolHeight / 3) * 2),
                          controlPoint2: CGPoint(x: currentShapeX + symbolWidth + curveXOffset, y: currentShapeY + symbolHeight / 3))
            path.addLine(to: CGPoint(x: currentShapeX, y: currentShapeY))
        }
        self.path = path
    }
    
    // Draw diamonds
    private func drawDiamonds(count: Int) {
        let allDiamondsWidth = CGFloat(countOfElements) * symbolWidth + CGFloat(countOfElements - 1) * symbolHorizontalMargin
        let beginX = (frame.size.width - allDiamondsWidth) / 2
        
        let path = UIBezierPath()
        
        for i in 0..<countOfElements {
            let currentShapeX = beginX + (symbolWidth * CGFloat(i)) + (CGFloat(i) * symbolHorizontalMargin)
            
            path.move(to: CGPoint(x: currentShapeX + symbolWidth / 2, y: symbolVerticalMargin))
            path.addLine(to: CGPoint(x: currentShapeX, y: symbolsCenter.y))
            path.addLine(to: CGPoint(x: currentShapeX + symbolWidth / 2, y: symbolVerticalMargin + symbolHeight))
            path.addLine(to: CGPoint(x: currentShapeX + symbolWidth, y: symbolsCenter.y))
            path.addLine(to: CGPoint(x: currentShapeX + symbolWidth / 2, y: symbolVerticalMargin))
        }
        
        self.path = path
    }
    
    // Draw ovals
    private func drawOvals(count: Int) {
        let allOvalsWidth = CGFloat(countOfElements) * symbolWidth + CGFloat(countOfElements - 1) * symbolHorizontalMargin
        let beginX = (frame.size.width - allOvalsWidth) / 2
        path = UIBezierPath()
        
        for i in 0..<countOfElements {
            let currentShapeX = beginX + (symbolWidth * CGFloat(i)) + (CGFloat(i) * symbolHorizontalMargin)

            path!.append(UIBezierPath(roundedRect: CGRect(x: currentShapeX,
                                                          y: symbolVerticalMargin,
                                                          width: symbolWidth,
                                                          height: symbolHeight),
                                      cornerRadius: symbolWidth))
        }
    }
    
    func updateBorderColor(state: BorderType) {
        switch state {
        case .normal: self.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        case .selected: self.layer.borderColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case .finded: self.layer.borderColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        }
    }
    
    
    func generateCardView(card: Card) {
        symbol = card.symbol
        countOfElements = card.numberOfElements
        shading = card.shading
        color = card.color
    }
}
