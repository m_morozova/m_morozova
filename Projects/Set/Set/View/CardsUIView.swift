//
//  CardsUIView.swift
//  Set
//
//  Created by Marina on 5/11/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class CardsUIView: UIView {
    
    var cardsButtons = [CardButton]()
    private var grid = Grid(layout: Grid.Layout.aspectRatio(5/3))
    
    override func layoutSubviews() {
        super.layoutSubviews()
        grid.frame = bounds
        grid.cellCount = cardsButtons.count
        
        for (index, button) in cardsButtons.enumerated() {
            guard let frame = grid[index] else { return }
            button.frame = frame
            addSubview(button)
        }
    }
    
    func addCards(numberOfCards: Int) {
        for _ in 0..<numberOfCards {
            cardsButtons.append(CardButton())
            addSubview(cardsButtons[cardsButtons.endIndex - 1])
        }
    }
    
    func removeButtons() {
        for card in cardsButtons {
            card.removeFromSuperview()
        }
    }
    
}
