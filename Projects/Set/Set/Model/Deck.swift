//
//  Deck.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

class Deck {
    
    var cards = [Card]()
    
    init() {
        for index in 0...80 {
            let card = cardCreater(index: index)
            cards += [card]
        }
        shuffleCards()
    }
    
    func getCards(numberOfCards: Int) -> [Card] {
        var returnedCards = [Card]()
        for _ in 0..<numberOfCards {
            guard let card = cards.first else { break }
            returnedCards.append(card)
            cards.remove(at: 0)
        }
        return returnedCards
    }
    
    func getCardsInstead(indexes: [Int], cardsOnPlay: [Card?]) -> [Card?] {
        var returnedCards = cardsOnPlay
        for index in indexes {
            returnedCards[index] = cards.count != 0 ? cards.remove(at: 0) : nil
        }
        return returnedCards
    }
    
    // Create an uniq card
    private func cardCreater(index: Int) -> Card {
        var card = Card()
        var identifier = index
        
        //Set symbol
        card.symbol = {
            switch index {
            case 0...26:
                return .diamond
            case 27...53:
                identifier -= 27
                return .squiggle
            case 54...80:
                identifier -= 54
                return .oval
            default:
                return .empthy
            }
        }()
        
        // Set color
        card.color = {
            switch identifier {
            case 0...8:
                return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            case 9...17:
                identifier -= 9
                return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            case 18...26:
                identifier -= 18
                return #colorLiteral(red: 0.3236978054, green: 0.1063579395, blue: 0.574860394, alpha: 1)
            default:
                return .white
            }
        }()
        
        // Set shading
        card.shading = {
            switch identifier {
            case 0...2:
                return .solid
            case 3...5:
                identifier -= 3
                return .striped
            case 6...8:
                identifier -= 6
                return .open
            default:
                return .empthy
            }
        }()
        
        // Set nuber of elements
        card.numberOfElements = identifier + 1
        
        return card
    }
    
    private func shuffleCards() {
        for index in cards.indices {
            let changingCardIndex = Int.random(in: 0..<cards.count)
            let changingCard = cards[index]
            cards[index] = cards[changingCardIndex]
            cards[changingCardIndex] = changingCard
        }
    }
    
}
