//
//  Set.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

protocol SetDelegate: AnyObject {
    func updateCard(index: Int, isSelected: Bool)
    func setUpNewCardsInstead(indexes: [Int])
    func showFindedSet(indexes: [Int])
    func updateDeckLabel(deck: Int)
    func updateTimerLabel(timeCount: Int)
    func updateBestTimeLabel(timeCount: Int)
}

class Set {
    static var bestTime = 0
    var deck = Deck()
    var cardsOnPlay = [Card?]()
    var indexOfSelectedCards = [Int]()
    weak var setDelegate: SetDelegate?
    var timer = Timer()
    var timeCounter = 0 {
        didSet {
            setDelegate?.updateTimerLabel(timeCount: timeCounter)
        }
    }
    var deckCount = 0 {
        didSet {
            setDelegate?.updateDeckLabel(deck: deckCount)
        }
    }
    
    init() {
        cardsOnPlay = deck.getCards(numberOfCards: 12)
        deckCount = deck.cards.count
    }
    
    deinit {
        timer.invalidate()
    }
    
    func addMoreCards() -> Bool {
        guard !deck.cards.isEmpty else { return false }
        cardsOnPlay += deck.getCards(numberOfCards: 3)
        deckCount = deck.cards.count
        return true
    }
    
    func selectCard(index: Int) {
        guard !cardsOnPlay[index]!.isSelected else {
            deleteFromSelection(cardIndex: index)
            return
        }
        guard indexOfSelectedCards.count < 3 else { return }
        indexOfSelectedCards.append(index)
        cardsOnPlay[index]!.isSelected = true
        setDelegate?.updateCard(index: index, isSelected: true)
        if indexOfSelectedCards.count == 3, checkMatching(indexOfSelectedCards) {
            closeSet()
        }
    }
    
    func findSet(check: Bool) -> Bool {
        for index1 in 0..<cardsOnPlay.count {
            if cardsOnPlay[index1] == nil { continue }
            for index2 in 1..<cardsOnPlay.count {
                if index1 == index2 || cardsOnPlay[index2] == nil { continue }
                for index3 in 2..<cardsOnPlay.count {
                    if index1 == index3 || index2 == index3 || cardsOnPlay[index3] == nil { continue }
                    let indexes = [index1, index2, index3]
                    if checkMatching(indexes) {
                        if !check { setDelegate?.showFindedSet(indexes: indexes) }
                        return true
                    }
                }
            }
        }
        return false
    }
    
    private func deleteFromSelection(cardIndex: Int) {
        cardsOnPlay[cardIndex]!.isSelected = false
        setDelegate?.updateCard(index: cardIndex, isSelected: false)
        for index in indexOfSelectedCards.indices {
            if indexOfSelectedCards[index] == cardIndex {
                indexOfSelectedCards.remove(at: index)
                break
            }
        }
    }
    
    private func closeSet() {
        cardsOnPlay = deck.getCardsInstead(indexes: indexOfSelectedCards, cardsOnPlay: cardsOnPlay)
        setDelegate?.setUpNewCardsInstead(indexes: indexOfSelectedCards)
        if deckCount > 0 {
            deckCount -= 3
        }
        indexOfSelectedCards = []
        if deck.cards.isEmpty, (Set.bestTime == 0 || timeCounter < Set.bestTime) && !findSet(check: true) {
            timer.invalidate()
            Set.bestTime = timeCounter
            timeCounter = 0
            setDelegate?.updateBestTimeLabel(timeCount: Set.bestTime)
        }
    }
    
    //MARK: -Checking Sets
    func checkMatching(_ indexes: [Int]) -> Bool {
        guard let card1 = cardsOnPlay[indexes[0]],
            let card2 = cardsOnPlay[indexes[1]],
            let card3 = cardsOnPlay[indexes[2]] else { return false }
        guard checkColors(card1, card2, card3),
            checkSymbols(card1, card2, card3),
            checkShadings(card1, card2, card3),
            checkNumbers(card1, card2, card3) else { return false }
        return true
    }
    
    private func checkColors(_ card1: Card, _ card2: Card, _ card3: Card) -> Bool {
        if card1.color == card2.color, card1.color == card3.color {
            return true
        } else if card1.color != card2.color, card1.color != card3.color, card2.color != card3.color {
            return true
        }
        return false
    }
    
    private func checkSymbols(_ card1: Card, _ card2: Card, _ card3: Card) -> Bool {
        if card1.symbol == card2.symbol, card1.symbol == card3.symbol {
            return true
        } else if card1.symbol != card2.symbol, card1.symbol != card3.symbol, card2.symbol != card3.symbol {
            return true
        }
        return false
    }
    
    private func checkShadings(_ card1: Card, _ card2: Card, _ card3: Card) -> Bool {
        if card1.shading == card2.shading, card1.shading == card3.shading {
            return true
        } else if card1.shading != card2.shading, card1.shading != card3.shading, card2.shading != card3.shading {
            return true
        }
        return false
    }
    
    private func checkNumbers(_ card1: Card, _ card2: Card, _ card3: Card) -> Bool {
        if card1.numberOfElements == card2.numberOfElements, card1.numberOfElements == card3.numberOfElements {
            return true
        } else if card1.numberOfElements != card2.numberOfElements, card1.numberOfElements != card3.numberOfElements, card2.numberOfElements != card3.numberOfElements {
            return true
        }
        return false
    }
    
}
