//
//  Card.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

struct Card {
    var symbol: Symbol
    var color: UIColor
    var shading: Shading
    var numberOfElements: Int
    var isSelected = false
    
    init() {
        self.symbol = .empthy
        self.color = .white
        self.shading = .empthy
        self.numberOfElements = 0
    }
}

enum Symbol {
    case diamond
    case squiggle
    case oval
    case empthy
}

enum Shading {
    case solid
    case striped
    case open
    case empthy
}
