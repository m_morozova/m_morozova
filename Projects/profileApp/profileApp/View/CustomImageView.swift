//
//  CustomImageView.swift
//  profileApp
//
//  Created by Marina on 4/18/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

class CustomImageView: UIImageView {
    func setUpPropertiesForProfileImageView() {
        self.contentMode = .scaleAspectFill
        self.layer.borderWidth = 10
        self.layer.borderColor = #colorLiteral(red: 0.8901960784, green: 0.7176470588, blue: 0.5568627451, alpha: 1)
        self.layer.masksToBounds = true
    }
}
