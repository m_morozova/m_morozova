//
//  SecondViewController.swift
//  profileApp
//
//  Created by Marina on 4/17/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit
import SnapKit

class SecondViewController: UIViewController {

    let photoImageView: UIImageView = {
        let imageView = CustomImageView()
        imageView.setUpPropertiesForProfileImageView()
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.2381048799, green: 0.2273015976, blue: 0.4683562517, alpha: 1)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.contentMode = .center
        label.font = UIFont(name: "Menlo-Bold", size: 30)
        return label
    }()
    
    let profile = Profile(name: "Marina Morozova", photoImage: UIImage(named: "mmv.jpg")!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
        self.setUpViews()
        self.setUpConstraints()
    }
    
    override func viewDidLayoutSubviews() { //called when view has just laid out its subviews
        super.viewDidLayoutSubviews()
        photoImageView.layer.cornerRadius = photoImageView.frame.width / 2 //recount radius when size of profileImage is changing (change orientation)
    }
    
    private func setUp() {
        self.view.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.831372549, blue: 0.6901960784, alpha: 1)
        self.title = "View from code"
        photoImageView.image = profile.photoImage
        nameLabel.text = profile.name
    }
    
    private func setUpViews() {
        self.view.addSubview(photoImageView)
        self.view.addSubview(nameLabel)
    }

    private func setUpConstraints() {
        photoImageView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5).priority(900)
            make.width.equalTo(photoImageView.snp.height)
            if #available(iOS 11.0, *) {
                make.top.greaterThanOrEqualTo(self.view.safeAreaLayoutGuide).offset(40)
                make.center.equalTo(self.view.safeAreaLayoutGuide)
            } else {
                make.top.greaterThanOrEqualTo(self.topLayoutGuide.snp.bottom).offset(40)
                make.center.equalToSuperview()
            }
        }
        nameLabel.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.leading.trailing.equalTo(self.view.safeAreaLayoutGuide).inset(20)
                make.bottom.lessThanOrEqualTo(self.view.safeAreaLayoutGuide).offset(-5)
            } else {
                make.leading.trailing.equalToSuperview().inset(20)
                make.bottom.lessThanOrEqualTo(self.bottomLayoutGuide.snp.top).offset(-5)
            }
            make.top.equalTo(photoImageView.snp.bottom).offset(60).priority(250)
            make.top.greaterThanOrEqualTo(photoImageView.snp.bottom).offset(5)
        }
    }
}
