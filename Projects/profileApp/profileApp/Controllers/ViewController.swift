//
//  ViewController.swift
//  profileApp
//
//  Created by Marina on 4/9/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageView: CustomImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    let profile = Profile(name: "Marina Morozova", photoImage: UIImage(named: "mmv.jpg")!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.setUpPropertiesForProfileImageView()
        nameLabel.text = profile.name
        profileImageView.image = profile.photoImage
    }
    
    override func viewDidLayoutSubviews() { //called when view has just laid out its subviews
        super.viewDidLayoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.frame.width / 2 //recount radius when size of profileImage is changing (change orientation)
    }
}


