//
//  Profile.swift
//  profileApp
//
//  Created by Marina on 4/9/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

struct Profile {
    let name: String
    let photoImage: UIImage
}
