//
//  PhotoViewCell.swift
//  ImageGallery
//
//  Created by Marina on 6/11/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    func setImage(url: URL, completion: @escaping (UIImage?) -> Void) {
        indicator.startAnimating()
        imageView.sd_setImage(with: url, completed: { image, _, _, _ in
            completion(image)
            self.indicator.stopAnimating()
        })
    }
}
