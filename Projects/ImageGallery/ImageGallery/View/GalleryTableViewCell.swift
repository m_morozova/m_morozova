//
//  GalleryTableViewCell.swift
//  ImageGallery
//
//  Created by Marina on 6/17/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

protocol GalleryTableViewCellDelegate: AnyObject {
    func setNewName(newName: String, in cell: GalleryTableViewCell)
}

class GalleryTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var onDoubleTap: (() -> Void)?
    var idexOfGallery = 0
    weak var delegate: GalleryTableViewCellDelegate?
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.delegate = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.isEnabled = false
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapAction))
        doubleTap.numberOfTapsRequired = 2
        self.addGestureRecognizer(doubleTap)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func doubleTapAction() {
        onDoubleTap?()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.textField.isEnabled = false
        guard let newName = textField.text, !newName.isEmpty else { return }
        delegate?.setNewName(newName: newName, in: self)
    }
    
}
