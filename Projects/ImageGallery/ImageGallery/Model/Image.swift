//
//  Image.swift
//  ImageGallery
//
//  Created by Marina on 6/6/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

struct Image: Codable {
    var url: URL?
    var aspectRatio: Float = 1
}
