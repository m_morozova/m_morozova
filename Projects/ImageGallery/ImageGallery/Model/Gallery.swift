//
//  Gallery.swift
//  ImageGallery
//
//  Created by Marina on 6/6/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

class Gallery: Codable {
    var name = String()
    var isDeleted = false
    var images = [Image]()
    
    init(name: String) {
        self.name = name
    }
    
    subscript(index: Int) -> Image {
        return images[index]
    }
}
