//
//  GalleriesRepository.swift
//  ImageGallery
//
//  Created by Marina on 6/11/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

class GalleriesRepository {
    
    private var allGalleries = [Gallery]() {
        didSet {
            self.saveData()
        }
    }
    var galleries: [Gallery] {
        return allGalleries.filter({ !$0.isDeleted })
    }
    var recentlyDeletedGalleries: [Gallery] {
        return allGalleries.filter({ $0.isDeleted })
    }
    
    init() {
        let service = JSONSevice()
        allGalleries = service.getDataFromJSON()
        if allGalleries.isEmpty {
            allGalleries.append(Gallery(name: "New Gallery"))
        }
    }
    
    func addGallery(name: String) {
        var isExicted = false
        for gallery in allGalleries where gallery.name == name {
            isExicted = true
        }
        if !isExicted { allGalleries.append(Gallery(name: name)) }
    }
    
    func renameGallery(index: Int, newName: String) {
        guard index < allGalleries.count else { return }
        allGalleries[index].name = newName
    }
    
    func updateGallery(newGallery: Gallery) {
        guard let index = allGalleries.firstIndex(where: { $0.name == newGallery.name }) else { return }
        allGalleries[index] = newGallery
    }
    
    func restoreGallery(index: Int) {
        recentlyDeletedGalleries[index].isDeleted = false
    }
    
    func deleteGallery(index: Int) {
        galleries[index].isDeleted = true
    }
    
    func completelyDeleteGallery(index: Int) {
        guard let indexToDelete = allGalleries.firstIndex(where: { $0.name == recentlyDeletedGalleries[index].name })
            else { return }
        allGalleries.remove(at: indexToDelete)
    }
    
    func getData() {
        let service = JSONSevice()
        allGalleries = service.getDataFromJSON()
    }
    
    func saveData() {
        let service = JSONSevice()
        service.saveDataToJSON(galleries: allGalleries)
    }
}
