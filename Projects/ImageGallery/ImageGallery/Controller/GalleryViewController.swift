//
//  GalleryTableViewController.swift
//  ImageGallery
//
//  Created by Marina on 6/6/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {
    
    enum Section: Int {
        case galleries, recentlyDeleted
    }
    
    private let permissionKey = "SavePermission"
    private var editableCell: GalleryTableViewCell?
    private var galleriesRepository = GalleriesRepository()
    private let defaults = UserDefaults.standard
    @IBOutlet var galleryTableView: UITableView!
    @IBOutlet weak var saveSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveSwitch.isOn = defaults.bool(forKey: permissionKey) ? true : false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if defaults.bool(forKey: permissionKey) {
            galleriesRepository.saveData()
        } else {
            CacheService.clearCache()
        }
    }
    
    @IBAction func saveSwitch(_ sender: UISwitch) {
        if saveSwitch.isOn {
            defaults.set(true, forKey: permissionKey)
        } else {
            defaults.set(false, forKey: permissionKey)
        }
    }
    
    @IBAction func addGallery() {
        addNewGalleryAlert()
    }
    
    // MARK: - Alert
    private func addNewGalleryAlert() {
        let alertController = UIAlertController(title: "Create new gallery", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter gallery name"
        }
        
        let saveAction = UIAlertAction(title: "Add", style: .default, handler: { _ in
            if let textField = alertController.textFields?[0] {
                if !textField.text!.isEmpty {
                    self.galleriesRepository.addGallery(name: textField.text!)
                    self.galleryTableView.reloadData()
                    self.galleriesRepository.saveData()
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { _ in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        alertController.preferredAction = saveAction
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Refresh collection view
    private func setNewGalleryToCollection(index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Gallery") as! GalleryDetailsViewController
        vc.gallery = galleriesRepository.galleries[index]
        splitViewController?.showDetailViewController(vc, sender: self)
    }
}

// MARK: - TableView settings (DataSource)
extension GalleryViewController: UITableViewDataSource {
    // Set number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currentSection = Section(rawValue: section) else { return 0 }
        switch currentSection {
        case .galleries:
            return galleriesRepository.galleries.count
        case .recentlyDeleted:
            return galleriesRepository.recentlyDeletedGalleries.count
        }
    }
    
    // Set cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! GalleryTableViewCell
        cell.textField.text = "??"
        if let currentSection = Section(rawValue: indexPath.section) {
            switch currentSection {
            case .galleries:
                cell.textField.text = galleriesRepository.galleries[indexPath.row].name
            case .recentlyDeleted:
                cell.textField.text = galleriesRepository.recentlyDeletedGalleries[indexPath.row].name
            }
        }
        cell.idexOfGallery = indexPath.row
        cell.delegate = self
        cell.onDoubleTap = {
            cell.textField.isEnabled = true
            cell.textField.becomeFirstResponder()
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // Set section's titles
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let currentSection = Section(rawValue: section) else { return nil }
        switch currentSection {
        case .galleries:
            return "Galleries"
        case .recentlyDeleted:
            return "Recently deleted galleries"
        }
    }
}

// MARK: - Actions with cells
extension GalleryViewController: UITableViewDelegate {
    
    // Choose gallery action
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 { return } // Does'n choose if row in section "recently deleted"
        setNewGalleryToCollection(index: indexPath.row)
        if let previosCell = editableCell {
            previosCell.textField.isEnabled = false
        }
    }
    
    // Delete action
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (_, _, _) in
            if let currentSection = Section(rawValue: indexPath.section) {
                switch currentSection {
                case .galleries:
                    self.galleriesRepository.deleteGallery(index: indexPath.row)
                case .recentlyDeleted:
                    self.galleriesRepository.completelyDeleteGallery(index: indexPath.row)
                }
            }
            self.galleryTableView.reloadData()
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    // Restore action
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 0 { return nil }
        let restoreAction = UIContextualAction(style: .destructive, title: "Restore") { (_,_,_)  in
            self.galleriesRepository.restoreGallery(index: indexPath.row)
            self.galleryTableView.reloadData()
        }
        restoreAction.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        let configuration = UISwipeActionsConfiguration(actions: [restoreAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
}

extension GalleryViewController: GalleryTableViewCellDelegate {
    func setNewName(newName: String, in cell: GalleryTableViewCell) {
        let index = cell.idexOfGallery
        galleriesRepository.renameGallery(index: index, newName: newName)
    }
}
