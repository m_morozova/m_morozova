//
//  GalleryDetailsViewController.swift
//  ImageGallery
//
//  Created by Marina on 6/7/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryDetailsViewController: UIViewController {
    
    var gallery: Gallery?
    private var width: CGFloat = 100
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if gallery == nil {
            gallery = GalleriesRepository().galleries.first
        }
        width = view.safeAreaLayoutGuide.layoutFrame.width / 3 - 20
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        width = view.safeAreaLayoutGuide.layoutFrame.width / 3 - 20
        imagesCollectionView.reloadData()
    }
    
    private func resaveGallery() {
        guard let newGallery = gallery else { return }
        let galleryRepository = GalleriesRepository()
        galleryRepository.updateGallery(newGallery: newGallery)
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "SavePermission") {
            galleryRepository.saveData()
        } else {
            CacheService.clearCache()
        }
    }
    
}

// MARK: - UICollectionView's protocols implemetation
extension GalleryDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gallery?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell",
                                                            for: indexPath) as! PhotoViewCell
        guard let gallery = self.gallery else { return cell }
        if let url = gallery[indexPath.item].url {
            cell.setImage(url: url, completion: { image in
                if let image = image, let index = gallery.images.firstIndex(where: { $0.url == url }) {
                    gallery.images[index].aspectRatio = Float(image.size.width / image.size.height)
                    collectionView.collectionViewLayout.invalidateLayout()
                }
            })
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let aspectRatio = gallery?[indexPath.item].aspectRatio else { return CGSize(width: 0, height: 0) }
        let imageHeight = width / CGFloat(aspectRatio)
        return CGSize(width: width, height: imageHeight)
    }
}

// MARK: - Drag and Drop implemetation
extension GalleryDetailsViewController: UICollectionViewDragDelegate, UICollectionViewDropDelegate {
    
    // Drag
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = imagesCollectionView
        return dragItem(at: indexPath)
    }
    
    private func dragItem(at indexPath: IndexPath) -> [UIDragItem] {
        guard let image = (imagesCollectionView.cellForItem(at: indexPath) as? PhotoViewCell)?.imageView.image else {
            print("no catch image")
            return []
        }
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = image
        return [dragItem]
    }
    
    //Drop
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                guard (item.dragItem.localObject as? UIImage) != nil else { continue }
                collectionView.performBatchUpdates({
                    guard let gallery = self.gallery else { return }
                    let draggedImage = gallery.images.remove(at: sourceIndexPath.item)
                    gallery.images.insert(draggedImage, at: destinationIndexPath.item)
                    imagesCollectionView.deleteItems(at: [sourceIndexPath])
                    imagesCollectionView.insertItems(at: [destinationIndexPath])
                })
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            } else {
                downloadNewImage(coordinator: item, at: destinationIndexPath.item)
                resaveGallery()
            }
        }
    }
    
    private func downloadNewImage(coordinator: UICollectionViewDropItem, at index: Int) {
        _ = coordinator.dragItem.itemProvider.loadObject(ofClass: URL.self) { (provider, _) in
            guard let url = provider?.imageURL, let gallery = self.gallery else { return }
            gallery.images.insert(Image(url: url, aspectRatio: 1), at: index)
            DispatchQueue.main.async {
                self.imagesCollectionView.performBatchUpdates({
                    self.imagesCollectionView.insertItems(at: [IndexPath(item: index, section: 0)])
                }, completion: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy,
                                            intent:  .insertAtDestinationIndexPath)
    }
    
}

extension GalleryDetailsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImageDisplay") as! ImageDisplayViewController
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoViewCell
        vc.image = cell.imageView.image
        if let gallery = self.gallery {
            vc.gallery = gallery
        }
        self.show(vc, sender: self)
    }
}
