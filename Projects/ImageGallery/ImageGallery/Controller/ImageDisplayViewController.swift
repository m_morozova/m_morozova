//
//  ImageDisplayViewController.swift
//  ImageGallery
//
//  Created by Marina on 6/17/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class ImageDisplayViewController: UIViewController, UIScrollViewDelegate {
    
    var gallery = Gallery(name: "")
    var imageView  = UIImageView()
    var autoZoomed = true
    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size
            autoZoomed = true
            zoomScaleToFit()
        }
    }
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 0.03
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        zoomScaleToFit()
    }
    
    @IBAction func backButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Gallery") as! GalleryDetailsViewController
        vc.gallery = self.gallery
        self.show(vc, sender: self)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        autoZoomed = false
    }
    
    func zoomScaleToFit() {
        if !autoZoomed { return }
        guard let imageScrollView = scrollView else { return }
        if image != nil &&
            (imageView.bounds.size.width > 0) &&
            (scrollView.bounds.size.width > 0) {
            let widthRatio = scrollView.bounds.size.width  / imageView.bounds.size.width
            let heightRatio = scrollView.bounds.size.height / self.imageView.bounds.size.height
            imageScrollView.zoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio
            imageScrollView.contentOffset = CGPoint(x: (imageView.frame.size.width - imageScrollView.frame.size.width) / 2,
                                                    y: (imageView.frame.size.height - imageScrollView.frame.size.height) / 2)
        }
    }
    
}
