//
//  SaveImageService.swift
//  ImageGallery
//
//  Created by Marina on 6/17/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import SDWebImage

class CacheService {
    static func clearCache() {
        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
    }
}
