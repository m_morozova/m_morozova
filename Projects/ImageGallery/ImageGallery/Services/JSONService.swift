//
//  JSONService.swift
//  ImageGallery
//
//  Created by Marina on 6/13/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

class JSONSevice {
    
    func getDataFromJSON() -> [Gallery] {
        guard let url = getPath(), let data = try? Data(contentsOf: url) else { return [Gallery]() }
        do {
            let items = try JSONDecoder().decode([Gallery].self, from: data)
            return items
        } catch let jsonErr {
            print("error ", jsonErr)
            return [Gallery]()
        }
    }
    
    func saveDataToJSON(galleries: [Gallery]) {
        guard let url = getPath() else { return }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let jsonData = try? encoder.encode(galleries)
        do {
            try jsonData?.write(to: url)
        } catch {
            print("Failed to write JSON data: \(error.localizedDescription)")
        }
    }
    
    private func getPath() -> URL? {
        let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let file = "Galleries.json"
        if let fileURL = directory?.appendingPathComponent(file) {
            return fileURL
        }
        return nil
    }
}
