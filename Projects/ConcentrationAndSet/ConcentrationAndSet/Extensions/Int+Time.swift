//
//  Int+Time.swift
//  Set
//
//  Created by Marina on 4/29/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

extension Int {
    var time: String {
        let minutes = self / 60
        let seconds = self % 60
        return "\(minutes):\(seconds < 10 ? "0" : "")\(seconds)"
    }
}
