//
//  Deck.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

class SetDeck {
    
    var cards = [SetCard]()
    
    init() {
        for index in 0...80 {
            let card = cardCreater(index: index)
            cards += [card]
        }
        shuffleCards()
    }
    
    func getCards(numberOfCards: Int) -> [SetCard] {
        var returnedCards = [SetCard]()
        for _ in 0..<numberOfCards {
            guard let card = cards.first else { break }
            returnedCards.append(card)
            cards.remove(at: 0)
        }
        return returnedCards
    }
    
    func getCardsInstead(indexes: [Int], cardsOnPlay: [SetCard?]) -> [SetCard?] {
        var returnedCards = cardsOnPlay
        for index in indexes {
            returnedCards[index] = !cards.isEmpty ? cards.remove(at: 0) : nil
        }
        return returnedCards
    }
    
    // Create an uniq card
    private func cardCreater(index: Int) -> SetCard {
        var card = SetCard()
        card.setAttributes(identifier: index)
        return card
    }
    
    private func shuffleCards() {
        for index in cards.indices {
            let changingCardIndex = Int.random(in: 0..<cards.count)
            let changingCard = cards[index]
            cards[index] = cards[changingCardIndex]
            cards[changingCardIndex] = changingCard
        }
    }
    
}
