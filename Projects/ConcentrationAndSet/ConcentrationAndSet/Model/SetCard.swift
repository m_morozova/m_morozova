//
//  Card.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

struct SetCard {
    var symbol: Symbol
    var color: UIColor
    var shading: Shading
    var numberOfElements: Int
    var isSelected = false
    
    init() {
        self.symbol = .empthy
        self.color = .white
        self.shading = .empthy
        self.numberOfElements = 0
    }
    
    mutating func setAttributes(identifier: Int) {
        var id = identifier
        setSymbol(identifier: &id)
        setColor(identifier: &id)
        setShading(identifier: &id)
        self.numberOfElements = id + 1
    }
    
    mutating func setSymbol(identifier: inout Int) {
        self.symbol = {
            switch identifier {
            case 0...26:
                return .diamond
            case 27...53:
                identifier -= 27
                return .squiggle
            case 54...80:
                identifier -= 54
                return .oval
            default:
                return .empthy
            }
        }()
    }
    
    mutating func setColor(identifier: inout Int) {
        self.color = {
            switch identifier {
            case 0...8:
                return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            case 9...17:
                identifier -= 9
                return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            case 18...26:
                identifier -= 18
                return #colorLiteral(red: 0.3236978054, green: 0.1063579395, blue: 0.574860394, alpha: 1)
            default:
                return .white
            }
        }()
    }
    
    mutating func setShading(identifier: inout Int) {
        self.shading = {
            switch identifier {
            case 0...2:
                return .solid
            case 3...5:
                identifier -= 3
                return .striped
            case 6...8:
                identifier -= 6
                return .open
            default:
                return .empthy
            }
        }()
    }
}

enum Symbol {
    case diamond
    case squiggle
    case oval
    case empthy
}

enum Shading {
    case solid
    case striped
    case open
    case empthy
}
