//
//  ViewController.swift
//  Set
//
//  Created by Marina on 4/24/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class SetViewController: UIViewController {
    
    // MARK: - Variables
    var game = Set()
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var deckLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var bestTimeLabel: UILabel!
    @IBOutlet weak var cardsUIView: SetCardsUIView!
    @IBOutlet weak var deckImageView: UIImageView!
    @IBOutlet weak var closedDeckImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleViewRotation(orientation: UIApplication.shared.statusBarOrientation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        game.timer.fire()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startGame()
        updateDeckLabel(deck: game.deckCount)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        game.timer.invalidate()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        handleViewRotation(orientation: toInterfaceOrientation)
    }
    
    // MARK: - Actions and gestures
    @IBAction func showSet() {
        let _ = game.findSet(check: false)
    }
    
    @IBAction func restartButton() {
        game = Set()
        startGame()
    }
    
    @objc func touchCard(recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state else { return }
        guard let cardButton = recognizer.view as? SetCardButton,
            cardsUIView.cardButtons.firstIndex(of: cardButton) != nil else { return }
        game.selectCard(index: cardsUIView.cardButtons.firstIndex(of: cardButton)!)
    }
    
    @objc func tapDeck(recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state else { return }
        dealMoreCardsButton()
    }
    
    @objc func swipeDownCards(recognizer: UISwipeGestureRecognizer) {
        dealMoreCardsButton()
    }
    
    private func addGestureRecognizer(cardButton: SetCardButton) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(touchCard(recognizer: )))
        cardButton.addGestureRecognizer(tap)
    }
    
    private func addDealCardsGestureRecognizer() {
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDownCards(recognizer: )))
        swipeGestureRecognizer.direction = .down
        view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    private func addDeckGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapDeck(recognizer: )))
        deckImageView.addGestureRecognizer(tap)
    }
    
    // MARK: - New game
    private func startGame() {
        game.setDelegate = self
        if Set.bestTime == 0 {
            bestTimeLabel.isHidden = true
        }
        deckImageView.isHidden = false
        closedDeckImageView.isHidden = true
        updateDeckLabel(deck: game.deckCount)
        startDeal()
        startTimer()
        addDealCardsGestureRecognizer()
        addDeckGestureRecognizer()
    }
    
    // MARK: - Deal cards
    private func startDeal() {
        cardsUIView.removeButtonsAndContainers()
        cardsUIView.cardButtons.removeAll()
        cardsUIView.cardContainers.removeAll()
        let imageFrame = deckImageView.superview!.convert(deckImageView.frame, to: cardsUIView)
        cardsUIView.addCards(numberOfCards: game.cardsOnPlay.count, deckFrame: imageFrame, complition: {})
        for index in 0..<game.cardsOnPlay.count {
            setUpCard(cardButton: cardsUIView.cardButtons[index], index: index)
        }
    }
    
    private func setUpCard(cardButton: SetCardButton, index: Int) {        
        cardButton.generateCardView(card: game.cardsOnPlay[index]!)
        cardButton.isHidden = false
        addGestureRecognizer(cardButton: cardButton)
    }
    
    func dealMoreCardsButton() {
        guard game.addMoreCards() else { return }
        let imageFrame = deckImageView.superview!.convert(deckImageView.frame, to: cardsUIView)
        cardsUIView.addCards(numberOfCards: 3, deckFrame: imageFrame, complition: {
            if self.game.deckCount == 0 {
                self.deckImageView.isHidden = true
            }
        })
        for index in game.cardsOnPlay.count-3..<game.cardsOnPlay.count {
            setUpCard(cardButton: cardsUIView.cardButtons[index], index: index)
        }
    }
    
    // MARK: - Timer
    func startTimer() {
        game.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            self.game.timeCounter += 1
        })
    }
    
    // MARK: - Rotation control
    private func handleViewRotation(orientation: UIInterfaceOrientation) {
        switch orientation {
        case .portrait, .portraitUpsideDown: setUpAxises(firstly: .vertical, secondary: .horizontal)
        case .landscapeLeft, .landscapeRight: setUpAxises(firstly: .horizontal, secondary: .vertical)
        default: break
        }
    }
    
    private func setUpAxises(firstly: NSLayoutConstraint.Axis, secondary: NSLayoutConstraint.Axis) {
        mainStackView.axis = firstly
        buttonsStackView.axis = secondary
    }
    
}

// MARK: - Extension. Delegation
extension SetViewController: SetDelegate {
    
    func updateCard(index: Int, isSelected: Bool) {
        cardsUIView.cardButtons[index].updateBorderColor(state: isSelected ? .selected : .normal)
    }
    
    func setUpNewCardsInstead(indexes: [Int]) {
        var delay = 1
        let imageFrame = deckImageView.superview!.convert(deckImageView.frame, to: cardsUIView)
        let closedDeckFrame = closedDeckImageView.superview!.convert(closedDeckImageView.frame, to: cardsUIView)
        view.sendSubviewToBack(closedDeckImageView)
        cardsUIView.completeSet(indexes: indexes, closedDeckFrame: closedDeckFrame, completion: {
            if self.closedDeckImageView.isHidden { self.closedDeckImageView.isHidden = false }
            for index in indexes {
                if let card = self.game.cardsOnPlay[index] {
                    self.cardsUIView.cardButtons[index].generateCardView(card: card)
                    self.updateCard(index: index, isSelected: card.isSelected)
                    self.cardsUIView.cardContainers[index].isHidden = false
                    self.cardsUIView.newCardAppear(index: index, deckFrame: imageFrame, delay: delay)
                    delay += 1
                } else {
                    self.cardsUIView.cardContainers[index].isHidden = true
                }
            }
        })
    }
    
    func showFindedSet(indexes: [Int]) {
        for index in indexes {
            cardsUIView.cardButtons[index].updateBorderColor(state: .finded)
        }
    }
    
    func updateDeckLabel(deck: Int) {
        deckLabel.text = "Deck: \(deck)"
        guard deck == 0 else { return }
    }
    
    func updateTimerLabel(timeCount: Int) {
        timerLabel.text = "\(timeCount.time)"
    }
    
    func updateBestTimeLabel(timeCount: Int) {
        bestTimeLabel.isHidden = false
        bestTimeLabel.text = "Best: \(timeCount.time)"
    }
    
}
