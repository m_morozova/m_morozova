//
//  ThemesViewController.swift
//  Concentration
//
//  Created by Marina on 5/28/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class ThemesViewController: UIViewController {

    @IBOutlet var themesButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tapThemeButton(_ sender: UIButton) {
        guard let themeIndex = themesButtons.firstIndex(of: sender) else { return }
        setUpTheme(themeIndex)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConG")
        splitViewController?.showDetailViewController(vc, sender: self)
    }
    
    func setUpTheme(_ themeIndex: Int) {
        let themes: [Theme] = [HalloweenTheme(), AnimalsTheme(), FacesTheme(),
                               TransportTheme(), FoodTheme(), FruitTheme()]
        ApplicationTheme.current = themes[themeIndex]
    }

}
