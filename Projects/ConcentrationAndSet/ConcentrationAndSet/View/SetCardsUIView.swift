//
//  CardsUIView.swift
//  Set
//
//  Created by Marina on 5/11/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

class SetCardsUIView: UIView {
    
    var cardContainers = [UIView]()
    var cardButtons = [SetCardButton]()
    private var grid = Grid(layout: Grid.Layout.aspectRatio(29/20))
    
    override func layoutSubviews() {
        super.layoutSubviews()
        grid.frame = bounds
        grid.cellCount = cardButtons.count
    }
    
    func addCards(numberOfCards: Int, deckFrame: CGRect, complition: @escaping () -> Void) {
        for _ in 0..<numberOfCards {
            addNewCard(deckFrame: deckFrame)
        }
        grid.cellCount = cardButtons.count
        for (index, container) in cardContainers.enumerated() {
            guard let frame = grid[index] else { return }
            let delay =  index - (cardContainers.count - numberOfCards)
            
            UIView.animate(
                withDuration: 0.5,
                delay: TimeInterval(delay) * 0.2,
                options: .curveEaseInOut,
                animations: {
                    container.frame = frame
                    self.cardButtons[index].frame = container.bounds
                },
                completion: { _ in complition()}
            )
        }
    }
    
    func completeSet(indexes: [Int], closedDeckFrame: CGRect, completion: @escaping () -> Void) {
        var counter = indexes.count
        for index in indexes {
            bringSubviewToFront(self.cardContainers[index])
            self.cardButtons[index].animateTurning(deckFrame: closedDeckFrame, completion: {
                UIView.animate(
                    withDuration: 0.5,
                    delay: 0.1,
                    options: .curveEaseInOut,
                    animations: {
                        self.cardContainers[index].frame = closedDeckFrame
                    },
                    completion: { _ in
                        self.exchangeSubviews(index: index)
                        counter -= 1
                        if counter == 0 {
                            completion()
                        }
                })
            })
        }
    }
    
    func newCardAppear(index: Int, deckFrame: CGRect, delay: Int) {
        self.cardContainers[index].frame = deckFrame
        self.cardButtons[index].frame = self.cardContainers[index].bounds
        guard let frame = grid[index] else { return }
        bringSubviewToFront(self.cardContainers[index])
        UIView.animate(
            withDuration: 0.5,
            delay: TimeInterval(delay) * 0.2,
            options: .curveEaseInOut,
            animations: {
                self.cardContainers[index].frame = frame
                self.cardButtons[index].frame = self.cardContainers[index].bounds
            },
            completion: nil
        )
    }
    
    func removeButtonsAndContainers() {
        for card in cardButtons {
            card.removeFromSuperview()
        }
        for container in cardContainers {
            container.removeFromSuperview()
        }
    }
    
    private func addNewCard(deckFrame: CGRect) {
        let button = SetCardButton()
        let container = UIView()
        button.backgroundColor = .white
        cardButtons.append(button)
        cardContainers.append(container)
        insertSubview(container, at: 0)
        container.insertSubview(button, at: 0)
        container.frame = deckFrame
        button.frame = container.bounds
    }
    
    private func exchangeSubviews(index: Int) {
        guard let imageView = self.cardContainers[index].subviews.first else { return }
        imageView.removeFromSuperview()
        self.cardContainers[index].addSubview(self.cardButtons[index])
    }
}
