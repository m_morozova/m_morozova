//
//  Theme.swift
//  Concentration
//
//  Created by Marina on 4/23/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation
import UIKit

protocol Theme {
    var emojis: [String] { get }
    var backgroundColor : UIColor { get }
    var cardColor : UIColor { get }
}

class ApplicationTheme {
    static var current: Theme = HalloweenTheme()
}

struct HalloweenTheme: Theme {
    var emojis: [String] {
        return ["👻", "🎃", "🧜🏻‍♀️", "🧚🏻‍♂️", "🏹", "🧟‍♂️", "🤴🏻", "💂🏼‍♀️"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
    }
}

struct AnimalsTheme: Theme {
    var emojis: [String] {
        return ["🦁", "🦄", "🐻", "🐯", "🐰", "🐼", "🦊", "🐹", "🐢", "🐨", "🐸", "🐶"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0.8886210322, green: 0.7160124183, blue: 0.5568627451, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 0.3383820653, green: 0.1822053492, blue: 0.6023293138, alpha: 1)
    }
}

struct FacesTheme: Theme {
    var emojis: [String] {
        return ["😀", "☺️", "😇", "🙃", "😎", "😜", "😡", "🤮", "😱", "😈"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0.5058823529, green: 0.6509803922, blue: 0.7647058824, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
    }
}

struct TransportTheme: Theme {
    var emojis: [String] {
        return ["🚗", "🚂", "🏍", "🚕", "🚌", "🚎", "🏎", "🚤", "🚁", "✈️", "🚲"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    }
}

struct FoodTheme: Theme {
    var emojis: [String] {
        return ["🍗", "🌭", "🍝", "🍕", "🍔", "🌮", "🍣", "🍟", "🍖", "🥪"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0.7411764706, green: 0.6823529412, blue: 0.5254901961, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 0.2862745098, green: 0.2078431373, blue: 0.1921568627, alpha: 1)
    }
}

struct FruitTheme: Theme {
    var emojis: [String] {
        return ["🍎", "🍋", "🍌", "🍉", "🍒", "🍑", "🍓", "🍇", "🥝", "🍍", "🍊"]
    }
    var backgroundColor: UIColor {
        return #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
    }
    var cardColor: UIColor {
        return #colorLiteral(red: 0.3088390101, green: 0.4433675182, blue: 0.109679005, alpha: 1)
    }
}
