//
//  ViewController.swift
//  Concentration
//
//  Created by Marina on 4/19/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import UIKit

extension ViewController: ConcentrationDelegate {
    func updateFlipsLabel(data: Int) {
        flipsLabel.text = "Flips: \(data)"
    }
    
    func updateScoreLabel(data: Int) {
        currentScoreLabel.text = "Your score: \(data)"
    }
    
    func updateBestScoreLabel(data: Int) {
        bestScoreLabel.text = "Best score: \(data)"
    }
}

class ViewController: UIViewController {

    let themes: [Theme] = [HalloweenTheme(), AnimalsTheme(), FacesTheme(), TransportTheme(), FoodTheme(), FruitTheme()]
    var themeColor = UIColor()
    var emojiChoices = [String]()
    var emoji = [Int: String]() 
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1)/2)
    
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipsLabel: UILabel!
    @IBOutlet weak var currentScoreLabel: UILabel!
    @IBOutlet weak var bestScoreLabel: UILabel!
    @IBOutlet weak var newGameButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game.delegate = self
        setUpTheme()
        updateTheme()
    }

    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        if !game.cards[cardNumber].isFacedUp {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
    }
    
    @IBAction func newGameButton(_ sender: UIButton) {
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1)/2)
        game.delegate = self
        setUpTheme()
        updateTheme()
        updateFlipsLabel(data: 0)
        updateScoreLabel(data: 0)
        for button in cardButtons {
            button.alpha = 1
        }
    }
    
    private func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFacedUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                if card.isMatched {
                    UIView.animate(withDuration: 1, animations: {
                        button.alpha = 0
                    })
                }
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = themeColor
            }
        }
    }
    
    //MARK: - Themes
    private func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            let randomIndex = randomNumber(to: emojiChoices.count)
            emoji[card.identifier] = emojiChoices.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
    
    private func setUpTheme() {
        let randomIndex = randomNumber(to: themes.count)
        emojiChoices = themes[randomIndex].emojis
        view.backgroundColor = themes[randomIndex].backgroundColor
        themeColor = themes[randomIndex].cardColor
        emoji = [:]
    }
    
    private func updateTheme() {
        flipsLabel.textColor = themeColor
        currentScoreLabel.textColor = themeColor
        bestScoreLabel.textColor = themeColor
        newGameButton.backgroundColor = themeColor
        updateViewFromModel()
    }
    
    private func randomNumber(to value: Int) -> Int {
        return Int.random(in: 0..<value)
    }

}
