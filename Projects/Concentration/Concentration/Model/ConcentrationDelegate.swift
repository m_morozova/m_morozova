//
//  ViewController+ConcentrationDelegate.swift
//  Concentration
//
//  Created by Marina on 4/23/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

protocol ConcentrationDelegate: AnyObject {
    func updateFlipsLabel(data: Int)
    func updateScoreLabel(data: Int)
    func updateBestScoreLabel(data: Int)
}
