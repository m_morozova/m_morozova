//
//  Concentration.swift
//  Concentration
//
//  Created by Marina on 4/22/19.
//  Copyright © 2019 Marina Morozova. All rights reserved.
//

import Foundation

class Concentration {
    
    var cards = [Card]()
    var indexOfFacedUpCard: Int?
    static var bestScore = 0
    weak var delegate: ConcentrationDelegate?
    var score = 0 {
        didSet {
            delegate?.updateScoreLabel(data: score)
        }
    }
    var flipCount = 0 {
        didSet {
            delegate?.updateFlipsLabel(data: flipCount)
        }
    }
    var matchedCards = 0 {
        didSet {
            if (matchedCards == cards.count) && (Concentration.bestScore == 0 || score > Concentration.bestScore) {
                Concentration.bestScore = score
                delegate?.updateBestScoreLabel(data: score)
            }
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        for _ in 0..<numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        shuffle()
    }
    
    func chooseCard(at index: Int) {
        guard !cards[index].isMatched else { return }
        guard let matchIndex = indexOfFacedUpCard else {
            for flipDownIndex in cards.indices {
                cards[flipDownIndex].isFacedUp = false
            }
            cards[index].isFacedUp = true
            flipCount += 1
            indexOfFacedUpCard = index
            return
        }
        guard matchIndex != index else { return }
        if cards[matchIndex].identifier == cards[index].identifier {
            cards[index].isMatched = true
            cards[matchIndex].isMatched = true
            score += 2
            matchedCards += 2
        }  else {
            score -= 1
        }
        cards[index].isFacedUp = true
        flipCount += 1
        indexOfFacedUpCard = nil
    }
    
    private func shuffle() {
        for index in cards.indices {
            let changingCardIndex = Int.random(in: 0..<cards.count)
            let changingCard = cards[index]
            cards[index] = cards[changingCardIndex]
            cards[changingCardIndex] = changingCard
        }
    }
}
